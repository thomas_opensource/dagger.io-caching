import random


def main():
    print(f"Output from Python randomnumber module: {random.randint(0, 10000000)}", flush=True)


if __name__ == "__main__":
    main()
