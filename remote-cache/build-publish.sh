#!/bin/bash

pip install -q -r requirements.txt

# https://github.com/dagger/dagger/pull/4543
# https://github.com/dagger/dagger/issues/3965#issuecomment-1414576096
export _EXPERIMENTAL_DAGGER_CACHE_CONFIG="type=registry,mode=max,ref=ttl.sh/dagger.io-caching/cache-1"

python3 build-publish.py
