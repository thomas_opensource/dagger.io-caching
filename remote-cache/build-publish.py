import os
import random
import sys

import anyio
import dagger


async def main():
    main_module = "randomnumber"

    work_dir = "../src"
    source_dir_name = main_module
    requirements_txt = "requirements.txt"

    dagger_cacheconfig_env = "_EXPERIMENTAL_DAGGER_CACHE_CONFIG"

    config = dagger.Config(workdir=work_dir, log_output=sys.stdout)

    async with dagger.Connection(config) as client:
        source_dir = client.host().directory(source_dir_name)
        requirements_file = client.host().directory(".", include=[requirements_txt]).file(requirements_txt)

        container = (
            client.container()
            .from_("python:3.11-slim-bullseye")

            .with_exec(["pip3", "install", "--upgrade", "pip"])
            .with_exec(["pip3", "install", "requests"])

            .with_workdir("/app")
            .with_file("requirements.txt", requirements_file)
            .with_exec(["pip3", "install", "-r", requirements_txt])

            .with_directory(main_module, source_dir)
            .with_default_args(["python3", "-m", main_module])
            .with_exec(["python3", "-m", main_module])
        )

        output = await container.stdout()

        image_ref = await (
            container.publish(f"ttl.sh/dagger.io-caching/remote-cache/{random.randint(0, 10000000)}:24h")
        )

    print(f"{dagger_cacheconfig_env}: {os.getenv(dagger_cacheconfig_env)}")
    print(f"Output generated in build: {output}")
    print(f"Published image to: {image_ref}")


anyio.run(main)
